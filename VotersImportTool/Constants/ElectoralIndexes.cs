﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VotersImportTool.Constants
{
    public static class ElectoralIndexes
    {
        public const int Province = 0;
        public const int Judiciary = 1;
        public const int Village = 2;
        public const int ReligionList = 3;
        public const int Gender = 4;
    }
}
