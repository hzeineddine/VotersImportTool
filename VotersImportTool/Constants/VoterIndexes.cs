﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VotersImportTool.Constants
{
    public static class VoterIndexes
    {
        public const int LastName = 0;
        public const int FirstName = 1;
        public const int MiddleName = 2;
        public const int MotherName = 3;
        public const int BirthDate = 4;
        public const int RegistrationNumber = 5;
        public const int Religion  = 6;
    }
}
