﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace VotersImportTool.Helpers
{
    public static class DataCleaningUtility
    {
        public static DateTime? ParseStringToDateTime(string dateString)
        {
            DateTime dateValue;
            var formatStrings = new string[] { "M/y", "M/d/y", "MM/dd/yy", "M-d-y", "yyyy", "yy" };
            if (DateTime.TryParseExact(dateString, formatStrings, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateValue))
                return dateValue;
            return null;
        }

        public static int ParseStringToInteger(string numberString)
        {
            try
            {
                string resultString = Regex.Match(numberString, @"\d+").Value;
                return Int32.Parse(resultString);
            }
            catch (Exception exception)
            {
                return 0;
            }
        }
    }
}
