namespace VotersImportTool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullableBirthDate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Voters", "BirthDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Voters", "BirthDate", c => c.DateTime(nullable: false));
        }
    }
}
