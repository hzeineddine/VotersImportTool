namespace VotersImportTool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Genders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Judiciaries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Province_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Provinces", t => t.Province_Id)
                .Index(t => t.Province_Id);
            
            CreateTable(
                "dbo.Provinces",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Villages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Judiciary_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Judiciaries", t => t.Judiciary_Id)
                .Index(t => t.Judiciary_Id);
            
            CreateTable(
                "dbo.ListReligions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Religions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Voters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        FatherName = c.String(),
                        LastName = c.String(),
                        MotherName = c.String(),
                        BirthDate = c.DateTime(nullable: false),
                        RegistrationNumber = c.Int(nullable: false),
                        Village_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Villages", t => t.Village_Id)
                .Index(t => t.Village_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Voters", "Village_Id", "dbo.Villages");
            DropForeignKey("dbo.Villages", "Judiciary_Id", "dbo.Judiciaries");
            DropForeignKey("dbo.Judiciaries", "Province_Id", "dbo.Provinces");
            DropIndex("dbo.Voters", new[] { "Village_Id" });
            DropIndex("dbo.Villages", new[] { "Judiciary_Id" });
            DropIndex("dbo.Judiciaries", new[] { "Province_Id" });
            DropTable("dbo.Voters");
            DropTable("dbo.Religions");
            DropTable("dbo.ListReligions");
            DropTable("dbo.Villages");
            DropTable("dbo.Provinces");
            DropTable("dbo.Judiciaries");
            DropTable("dbo.Genders");
        }
    }
}
