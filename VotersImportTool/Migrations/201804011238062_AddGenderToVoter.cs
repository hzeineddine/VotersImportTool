namespace VotersImportTool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGenderToVoter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Voters", "Gender_Id", c => c.Int());
            CreateIndex("dbo.Voters", "Gender_Id");
            AddForeignKey("dbo.Voters", "Gender_Id", "dbo.Genders", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Voters", "Gender_Id", "dbo.Genders");
            DropIndex("dbo.Voters", new[] { "Gender_Id" });
            DropColumn("dbo.Voters", "Gender_Id");
        }
    }
}
