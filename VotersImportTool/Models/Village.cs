﻿namespace VotersImportTool.Models
{
    public class Village
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Judiciary Judiciary { get; set; }
    }
}