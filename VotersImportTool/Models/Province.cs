﻿using System.Collections.Generic;

namespace VotersImportTool.Models
{
    public class Province
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Judiciary> Judiciaries { get; set; }
    }
}