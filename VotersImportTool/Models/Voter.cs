﻿using System;

namespace VotersImportTool.Models
{
    public class Voter
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string FatherName { get; set; }
        public string LastName { get; set; }
        public string MotherName { get; set; }
        public DateTime? BirthDate { get; set; }
        public int RegistrationNumber { get; set; }
        public Gender Gender { get; set; }
        public Village Village { get; set; }
    }
}