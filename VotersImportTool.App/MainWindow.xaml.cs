﻿using System.Collections.Generic;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;
using VotersImportTool.App.ViewModels;
using VotersImportTool.Helpers;

namespace VotersImportTool.App
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string FileName { get; set; }
        public string[] Files { get; set; }
        public List<CsvFilesViewModel> FilesViewModel { get; set; }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.DefaultExt = ".csv";
            dialog.Filter = "CSV Files (*.csv,*.xlsx)|*.csv;*.xlsx";
            dialog.Multiselect = true;
            if (dialog.ShowDialog() == true)
            {
                FileName = dialog.FileName;
                Files = dialog.FileNames;
                FilesViewModel = new List<CsvFilesViewModel>();
                foreach (string fileName in dialog.FileNames)
                {
                    FilesViewModel.Add(new CsvFilesViewModel()
                    {
                        Name = System.IO.Path.GetFileName(fileName),
                        Path = fileName
                    });
                }
                FileNamesComboBox.ItemsSource = FilesViewModel;
                FileNamesComboBox.SelectedValue = FilesViewModel[0].Path;

               
            }
        }

        private void BtnQuery_Click(object sender, RoutedEventArgs e)
        {
            string query = QueryText.Text;
            if (!string.IsNullOrEmpty(query) && null != FileName)
            {
                DataTable table = DataHelper.GetDataTableFromCsv(FileName, query);
                if (table.Columns.Count == 0)
                    MessageBox.Show("Error!");
                else
                    DataGrid.ItemsSource = table.DefaultView;
            }
        }

        private void FileNamesComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CsvFilesViewModel csvFile = (sender as ComboBox)?.SelectedItem as CsvFilesViewModel;
            if (null != csvFile)
            {
                QueryText.Text = $"Select * from [{csvFile.Name}];";
                DataTable table = DataHelper.GetDataTableFromCsv(csvFile.Path);
                if (table.Columns.Count == 0)
                    MessageBox.Show("Error!");
                else
                    DataGrid.ItemsSource = table.DefaultView;
            }
        }

        private void ImportButton_Click(object sender, RoutedEventArgs e)
        {
            if (null != Files && Files.Length > 0)
            {
                DataHelper.FillProvince(Files);
                MessageBox.Show("Done");
            }
        }
    }

}
